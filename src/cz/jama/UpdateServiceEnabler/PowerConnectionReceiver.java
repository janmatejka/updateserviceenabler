package cz.jama.UpdateServiceEnabler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * @author Jan Matějka
 */
public class PowerConnectionReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        /*
        int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;

        int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
        */
        //android.intent.action.ACTION_POWER_DISCONNECTED
        //android.intent.action.ACTION_POWER_CONNECTED

        String action=intent.getAction();
        if("android.intent.action.ACTION_POWER_DISCONNECTED".equals(action))
            Management.onPower(false,context);
        else if ("android.intent.action.ACTION_POWER_CONNECTED".equals(action))
            Management.onPower(true,context);
    }
}
