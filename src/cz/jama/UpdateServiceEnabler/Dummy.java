package cz.jama.UpdateServiceEnabler;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * @author Jan Matějka
 */
public class Dummy extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Management.enableServices(this);
        Management.startService(this);
        Management.setLimitBacgroundProcesses(this, Management.NUMBER_BACKGROUND_TASKS);
        setContentView(R.layout.main);
        final Button buttExit=(Button) findViewById(R.id.buttonExit);
        buttExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                stopService(new Intent(Dummy.this, UpdateServiceEnablerService.class));
            }
        });
    }
}