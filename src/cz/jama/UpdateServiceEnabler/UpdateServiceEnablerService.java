package cz.jama.UpdateServiceEnabler;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.widget.Toast;

/**
 * @author Jan Matějka
 */
public class UpdateServiceEnablerService extends Service {
    //NotificationManager mNM;

    private ScreenStateReceiver mScreenStateReceiver;

    @Override
    public void onCreate() {
        super.onCreate();

        IntentFilter screenStateFilter = new IntentFilter();
        screenStateFilter.addAction(Intent.ACTION_SCREEN_ON);
        screenStateFilter.addAction(Intent.ACTION_SCREEN_OFF);
        mScreenStateReceiver=new ScreenStateReceiver();
        registerReceiver(mScreenStateReceiver, screenStateFilter);
        showNotification();
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mScreenStateReceiver);
        Toast.makeText(this, R.string.service_stopped, Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private void showNotification() {
        // In this sample, we'll use the same text for the ticker and the expanded notification
        CharSequence text = getText(R.string.service_started);

        Notification notification = new Notification(R.drawable.ic_launcher, "Conn saver", System.currentTimeMillis());

        Intent intent = new Intent(this, Dummy.class);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);

        //Set the info for the views that show in the notification panel.
        notification.setLatestEventInfo(this, "Conn saver", "Je spuštěn", contentIntent);
        startForeground(100, notification); //tímto Androidu mj. oznámíme, že naše aplikace je důležitá a nemá ji zabíjet
        //mNM.notify(R.string.service_started, notification);
    }
}
