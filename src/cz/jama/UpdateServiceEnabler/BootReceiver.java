package cz.jama.UpdateServiceEnabler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReceiver extends BroadcastReceiver {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        Management.enableServices(context);
        Management.startService(context);
        Management.setLimitBacgroundProcesses(context, Management.NUMBER_BACKGROUND_TASKS);
    }

}
