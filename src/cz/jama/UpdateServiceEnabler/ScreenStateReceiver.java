package cz.jama.UpdateServiceEnabler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * @author Jan Matějka
 */
public class ScreenStateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
    if(intent.getAction().equals("android.intent.action.SCREEN_OFF"))
        Management.onScreen(false, context);
    else if(intent.getAction().equals("android.intent.action.SCREEN_ON"))
        Management.onScreen(true, context);
    }
}
