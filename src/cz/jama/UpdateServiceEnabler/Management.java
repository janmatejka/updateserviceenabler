package cz.jama.UpdateServiceEnabler;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author Jan Matějka
 */
public class Management {

    public static final int NUMBER_BACKGROUND_TASKS = 2;

    static void onPower(boolean connected,Context context)
    {
        //Toast.makeText(context,"Power = " + String.valueOf(connected),Toast.LENGTH_SHORT).show();
        if(connected)
            enableWifi(true,context);
    }

    static void startService(Context context) {
        Intent i = new Intent(context, UpdateServiceEnablerService.class);
        //i.putExtra("KEY1", "Value to be used by the service");
        context.startService(i);
    }

    static void enableServices(Context context) {
        try {
            Log.d("UpdateServiceEnabler", "enableServices{");
            RunAsRoot(new String[]{
                    "pm enable com.google.android.gms/.update.SystemUpdateActivity"
                    , "pm enable com.google.android.gms/.update.SystemUpdateService"
                    , "pm enable com.google.android.gms/.update.SystemUpdateService$ActiveReceiver"
                    , "pm enable com.google.android.gms/.update.SystemUpdateService$Receiver"
                    , "pm enable com.google.android.gms/.update.SystemUpdateService$SecretCodeReceiver"
                    , "pm enable com.google.android.gsf/.update.SystemUpdateActivity"
                    , "pm enable com.google.android.gsf/.update.SystemUpdatePanoActivity"
                    , "pm enable com.google.android.gsf/.update.SystemUpdateService"
                    , "pm enable com.google.android.gsf/.update.SystemUpdateService$Receiver"
                    , "pm enable com.google.android.gsf/.update.SystemUpdateService$SecretCodeReceiver"
            });
            Log.d("UpdateServiceEnabler", "enableServices}");

            Toast.makeText(context, "Povoleno UpdateService!", Toast.LENGTH_SHORT).show();

        } catch (IOException e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private static void RunAsRoot(String[] cmds) throws IOException {
        Process p = Runtime.getRuntime().exec("su");
        DataOutputStream os = new DataOutputStream(p.getOutputStream());
        for (String tmpCmd : cmds) {
            os.writeBytes(tmpCmd + "\n");
        }
        os.writeBytes("exit\n");
        os.flush();
    }

    public static void onScreen(boolean screenOn, Context context) {
        if(screenOn)
            enableWifi(true, context);
    }

    private static void enableWifi(boolean enable, Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(enable);
        Toast.makeText(context, "Wifi povoleno", Toast.LENGTH_SHORT).show();
    }

    public static void setLimitBacgroundProcesses(Context context, int n) {
        try {
            Log.d("UpdateServiceEnabler", "setLimitBacgroundProcesses{");
            String cmd = "service call activity 51 i32 " + Integer.toString(n);
            RunAsRoot(new String[]{
                    cmd
            });
            Log.d("UpdateServiceEnabler", "setLimitBacgroundProcesses}");

            Toast.makeText(context, "Limit procesů nastaven!", Toast.LENGTH_SHORT).show();

        } catch (IOException e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }
}
